import blurCheck
import cv2


def resizeImage(image, width=None, height=None):
    heightImage, widthImage = image.shape[:2]
    if width is None and height is None:
        dim = None
    if width is None:
        factor = height / heightImage
        dim = (int(widthImage * factor), height)
    else:
        factor = width / widthImage
        dim = (width, int(heightImage * factor))
    return cv2.resize(image, dim)


image = cv2.imread('temp.jpg', cv2.IMREAD_COLOR)
# imageBlur1 = cv2.GaussianBlur(image, (3, 3), 0)
# imageBlur2 = cv2.GaussianBlur(image, (9, 9), 0)

cv2.imshow("Image", resizeImage(blurCheck.determination_of_degree_blur(image), None, 700))
# cv2.imshow("ImageBlur1", resizeImage(blurCheck.determination_of_degree_blur(imageBlur1), None, 700))
# cv2.imshow("ImageBlur2", resizeImage(blurCheck.determination_of_degree_blur(imageBlur2), None, 700))
key = cv2.waitKey(0)

# video = cv2.VideoCapture(0)
# if video.isOpened():
#     ret, frame = video.read()
# else:
#     ret = False
# while ret:
#     ret, frame = video.read()
#     image = cv2.flip(frame, 1)
#     cv2.imshow("Image", blurCheck.determination_of_degree_blur(image))
#     if cv2.waitKey(1) == 27:
#         break
# cv2.destroyAllWindows()
# video.release()
