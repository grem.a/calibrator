import cv2


def crop_image(image, startXArea, startYArea, endXArea, endYArea):
    return image[startYArea: endYArea, startXArea: endXArea]


def variance_of_laplacian(cropImage):
    return cv2.Laplacian(cropImage, cv2.CV_32F).var()


def determination_of_degree_blur(image, areaPercent=70):
    areaPercent = areaPercent if 100 > areaPercent > 0 else 70
    areaPercent = areaPercent / 100

    heightImage, widthImage = image.shape[:2]

    startXArea, startYArea = int((widthImage - widthImage * areaPercent) / 2), \
                             int((heightImage - heightImage * areaPercent) / 2)
    endXArea, endYArea = int((widthImage + widthImage * areaPercent) / 2), \
                         int((heightImage + heightImage * areaPercent) / 2)

    cropImage = crop_image(image, startXArea, startYArea, endXArea, endYArea)

    grayCropImage = cv2.cvtColor(cropImage, cv2.COLOR_BGR2GRAY)
    focusMeasure = variance_of_laplacian(grayCropImage)

    cv2.rectangle(image, (startXArea, startYArea), (endXArea, endYArea), (0, 255, 0), 3)
    cv2.putText(image, "{:.2f}".format(focusMeasure), (10, 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)

    return image